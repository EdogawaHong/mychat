package com.example.mychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mychat.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

public class RegisterActivity extends AppCompatActivity {

    Button btn_register;
    MaterialEditText tv_username,tv_email,tv_password;
    FirebaseAuth auth;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn_register=findViewById(R.id.register);
        tv_username=findViewById(R.id.username);
        tv_email=findViewById(R.id.email);
        tv_password=findViewById(R.id.password);

        auth=FirebaseAuth.getInstance();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt_username =tv_username.getText().toString();
                String txt_email=tv_email.getText().toString();
                String txt_password=tv_password.getText().toString();
                if (TextUtils.isEmpty(txt_username)||TextUtils.isEmpty(txt_email)||TextUtils.isEmpty(txt_password)){
                    Toast.makeText(RegisterActivity.this,"Yêu cầu nhập đủ thông tin!",Toast.LENGTH_SHORT).show();
                }else {
                    if (txt_password.length()<6){
                        Toast.makeText(RegisterActivity.this,"Password phải có ít nhất 6 kí tự",Toast.LENGTH_SHORT).show();
                    }else {
                        register(txt_username,txt_email,txt_password);
                    }
                }
            }
        });
    }
    private void register(final String username, String email, String password){
        auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if ((task.isSuccessful())){
                            FirebaseUser firebaseUser=auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid=firebaseUser.getUid();
                            User user=new User(userid,username,"emoji36");
                            reference= FirebaseDatabase.getInstance().getReference().child("Users").child(userid);
                            reference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent=new Intent(RegisterActivity.this,MainChatActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }}
                            });
                        }else {
                            Toast.makeText(RegisterActivity.this,
                                    "Bạn không thể đăng ký tài khoản mới với email hoặc password này!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}