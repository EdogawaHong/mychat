package com.example.mychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.mychat.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    ImageView change_image;
    CircleImageView profile_image;
    String image_name = "";

    EditText edt_username, edt_email, edt_password;
    Button btn_update, btn_cancel;

    DatabaseReference reference;
    FirebaseUser fuser;

    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, MainChatActivity.class));
                finish();
            }
        });

        change_image = findViewById(R.id.change_image);
        profile_image = findViewById(R.id.profile_image);
        edt_username = findViewById(R.id.username);
        edt_email = findViewById(R.id.email);
        edt_password = findViewById(R.id.password);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_update = findViewById(R.id.btn_update);

        fuser = FirebaseAuth.getInstance().getCurrentUser();

        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());
        loadInfo();

        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogImage();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!image_name.equals(""))
                    reference.child("image").setValue(image_name);
                if (!edt_username.getText().toString().equals(username) && !edt_username.equals("")) {
                    reference.child("username").setValue(edt_username.getText().toString());
                }
                if (!edt_password.equals("")) {
                    fuser.updatePassword(edt_password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Toast.makeText(getBaseContext(),"Cập nhật thành công!",Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getBaseContext(),"Có lỗi xảy ra!",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadInfo();
            }
        });
    }

    private void loadInfo(){
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                edt_username.setText(user.getUsername());
                username = user.getUsername();
                int idImage = getResources().getIdentifier(user.getImage(), "drawable", getPackageName());
                profile_image.setImageResource(idImage);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        for (UserInfo profile : fuser.getProviderData()) {
            edt_email.setText(profile.getEmail());
        }
        edt_password.setText("********");
    }

    private void DialogImage() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_image);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        Button btn_huy = dialog.findViewById(R.id.btn_huy);
        TableLayout tableLayout = dialog.findViewById(R.id.tableImage);
        int dong = 8, cot = 6;
        int vt = 0;
        for (int i = 0; i < dong; i++) {
            TableRow tableRow = new TableRow(this);
            for (int j = 0; j < cot; j++) {
                ImageView imageView = new ImageView(this);
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(125, 125);
                imageView.setLayoutParams(layoutParams);
                vt++;
                final int idHinh = this.getResources().getIdentifier("emoji" + vt, "drawable", getPackageName());
                imageView.setImageResource(idHinh);
                tableRow.addView(imageView);
                final int finalVt = vt;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profile_image.setImageResource(idHinh);
                        image_name = "emoji" + finalVt;
                        dialog.dismiss();
                    }
                });
            }
            tableLayout.addView(tableRow);
        }
        btn_huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}